from lfc_models import lfc_ui
from lfc_farid import lfc_farid
import collections
import math
import numbers
import numpy

from abc import ABCMeta, abstractmethod
from Units import *


class linspace(collections.Sequence):
    """linspace(start, stop, num) -> linspace object

    Return a virtual sequence of num numbers from start to stop (inclusive).

    If you need a half-open range, use linspace(start, stop, num+1)[:-1].
    """

    def __init__(self, start, stop, num=None, step=None):
        """ The linspace constructor. """

        self.__start, self.__stop = start, stop

        if num is not None and step is not None:
            raise ValueError("Only 'num' or 'step' can be given, not both.")

        if num is not None:
            if not isinstance(num, numbers.Integral) or num < 1:
                raise ValueError('num must be an integer > 1')
            self.__num = num
            if self.__start == self.__stop:
                self.__step = self.__stop - self.__start
            else:
                self.__step = (self.__stop - self.__start)/(self.__num - 1)
        elif step is not None:
            self.__step = step
            if self.__stop == self.__start:
                self.__num = 1
            self.__num = int(((self.__stop - self.__start)/self.__step).m) + 1

    def __len__(self):
        return self.num

    def __getitem__(self, i):
        if isinstance(i, slice):
            return [self[x] for x in range(*i.indices(len(self)))]
        if i < 0:
            i = self.num + i
        if i >= self.num:
            raise IndexError('linspace object index out of range')
        if i == self.num-1:
            return self.stop
        return self.start + i*self.step

    def __repr__(self):
        return '{}({}, {}, {}, {})'.format(type(self).__name__, self.start, self.stop, self.num, self.step)

    def __eq__(self, other):
        if not isinstance(other, linspace):
            return False
        return ((self.start, self.stop, self.num, self.step) ==
                (other.start, other.stop, other.num, other.step))

    def __ne__(self, other):
        return not self==other

    def __hash__(self):
        return hash((type(self), self.start, self.stop, self.num, self.step))

    @property
    def step(self):
        return self.__step

    @property
    def stop(self):
        return self.__stop

    @property
    def start(self):
        return self.__start

    @property
    def num(self):
        return self.__num

class Plasma():
    """ """
    def __init__(self,
            elements=None,
            electron_density=None,
            electron_temperature=None,
            average_ionization=None,
            ):
        """ """

        self.elements = elements
        self.electron_density = electron_density
        self.electron_temperature = electron_temperature
        self.average_ionization = average_ionization

        # Calculate plasma parameters
        self.plasma_frequency = numpy.sqrt( electron_density * e**2 / m_e / epsilon0)
        self.wple = self.plasma_frequency
        self.wigner_seitz_radius = (3./4./math.pi/electron_density)**(1/3)
        self.rs = (self.wigner_seitz_radius / bohr).m_as('dimensionless')
        self.gamma_ee = (e**2 / (4.*math.pi*epsilon0*self.wigner_seitz_radius)).m_as('joule') / (kB*electron_temperature).m_as('joule')

        alphaFermi = (4./9./math.pi)**(1/3)
        self.fermi_wavenumber = 1./alphaFermi/self.wigner_seitz_radius
        self.kF = self.fermi_wavenumber
        self.qF = self.fermi_wavenumber

        self.debye_length = numpy.sqrt( epsilon0  * electron_temperature * kB / electron_density / e**2 )
        self.debye_kappa =1./self.debye_length

class LFC():

    @classmethod
    def _static_models(cls):
        return [
                Farid,
                Utsumi1982,
                #'Vashishta-Pathak',
                #'STLS',
                #'Debye-Hueckel',
                Hubbard,
                ]
    @classmethod
    def _dynamic_models(cls):
        return [
                #'Dabrowski',
                ]

    @classmethod
    def _supported_models(cls):
        return cls._static_models() + cls._dynamic_models()

    def __init__(self, model, plasma, wavenumbers, energies):
        """ """
        self.model = model
        self.plasma = plasma

        self.wavenumbers  = wavenumbers
        self.energies = energies

        self.__values = None

        self.__calculate_values()

    @property
    def energies(self):
        """ """
        return self.__energies
    @energies.setter
    def energies(self, val):
        """ """
                # Check units
        if not compatible(val,joule):
            raise ValueError("Parameter 'energies' has the wrong unit", str(val.units))
        self.__energies = val

    @property
    def model(self):
        """ """
        return self.__model
    @model.setter
    def model(self, val):
        """ """

        if not issubclass(val, LFCModel):
            raise TypeError("Incorrect type. Expected LFCModel, obtained ", val)
        if not val in self._supported_models():
            raise ValueError("Model %s is currently not supported. Supported models: %s" % (val, repr(self._supported_models())))

        self.__model = val

    @property
    def plasma(self):
        """ """
        return self.__plasma
    @plasma.setter
    def plasma(self, val):
        """ """
        if isinstance(val, Plasma):
            self.__plasma = val
        else:
            raise TypeError("Incorrect type. Expected Plasma, obtained ", type(val))

    def __calculate_values(self):

        self.__values = self.model.calculateLFC(self.wavenumbers, self.energies, self.plasma)

    def evaluate(self, wavenumber=None, energy=None):
        """ Return the computed LFC values. """

        if wavenumber is None and energy is None:
            return self.__values

        #if wavenumber in self.wavenumbers:
            #return self.__values[self.wavenumbers==wavenumber]

        #if wavenumber > self.wavenumbers[0] and wavenumber < self.wavenumbers[-1]:
            #raise NotImplementedError()
        else:
            return self.model.calculateLFC(wavenumber, energy, self.plasma)


### LFC Model singletons

class LFCModel(object, metaclass=ABCMeta):
    """ """

    @classmethod
    def name(cls):
        pass

    @classmethod
    def reference(cls):
        pass

class Hubbard(LFCModel, metaclass=ABCMeta):
    """ Implements the Hubbard model LFC. [J. Hubbard, Proc. R. Soc. London Ser. A *243*, 336 (1957)].  """

    @classmethod
    def calculateLFC(cls, q, w, plasma):
        qF = plasma.qF
        return 0.5*q**2/(q**2 + qF**2)

    @classmethod
    def name(cls):
        return "Hubbard"

    @classmethod
    def reference(cls):
        return "J. Hubbard, Proc. R. Soc. London Ser. A *243*, 336 (1957)."

class Farid(LFCModel, metaclass=ABCMeta):
    """ Implements the LFC model by Farid et al. Phys. Rev. B, *48*, 11602 (1993)."""

    @classmethod
    def calculateLFC(cls, q, w, plasma):

        rs = plasma.rs
        qs = q.m_as(1/bohr)

        return lfc_farid(rs, qs)

    @classmethod
    def name(cls):
        return "Farid"

    @classmethod
    def reference(cls):
        return "Farid et al. Phys. Rev. B, *48*, 11602 (1993)."

class Utsumi1982(LFCModel, metaclass=ABCMeta):
    """ Implements the LFC model by Utsumi & Ichimaru Phys. Rev. A, *26*, 603 (1982)."""

    @classmethod
    def calculateLFC(cls, q, w, plasma):

        rs = plasma.rs
        qs = q.m_as(1/bohr)

        return lfc_ui(rs, qs)

    @classmethod
    def name(cls):
        return "Utsumi1982"

    @classmethod
    def reference(cls):
        return "Utsumi & Ichimaru Phys. Rev. A, *26*, 603 (1982)."


