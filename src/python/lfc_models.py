from math import pi as M_PI
from math import sqrt
import numpy


from scipy.special import i1 as besselI1

def pow(a,b):
    return a**b

# Farid, Eq. (18).
b = [0.0310907*2.0,   # Factor two: Inconsistency between Farid et al (0.031...) and UI (0.06...). Latter is correcy.
     9.81379,
     2.282224,
     0.736411,
     ]

lmbda = pow(4./9./M_PI,1./3.)        #p. 11606 top, right column

def dEdrs(rs):
    x = sqrt(rs)
    return b[0]/rs*(1.+b[1]*x)/(1.+b[1]*x+b[2]*rs+b[3]*rs*x) #(18)

def d2Edrs2(rs):
    x = sqrt(rs)
    return -b[0]/2./pow(rs,2)*(2.+2.*pow(b[1],2)*rs+4.*b[2]*rs+5.*b[3]*rs*x+b[1]*x*(4.+3.*b[2]*rs+4.*b[3]*rs*x))/pow(1.+b[1]*x+b[2]*rs+b[3]*rs*x,2)        #follows from (18)

def gamma0(rs):
    return 0.25-M_PI*lmbda/24.*(pow(rs,3)*d2Edrs2(rs)-2.*pow(rs,2)*dEdrs(rs))    #(17)

def g0(rs):
    z = 4.*sqrt(lmbda*rs/M_PI)        #(16) and following text
    Iz = besselI1(z)

    return 0.125*pow(z/Iz,2)


def lfc_ui(rs,k):
    """
    :param rs: The dimensionless electron gas parameter rsaB = (3/4\pi ne)^1/3
    :param k: The wavenumber(s) in units of inverse aB

    :raises: NotImplementedError
    """

    raise NotImplementedError("The Utsumi-Ichimaru model is not implemented yet.")

    kF = 1./lmbda/rs            #before (15a)
    EF = kF*kF                #trivial
    wp = sqrt(12./pow(rs,3))            #from wpl = 16\pi*n, n=3/4pi rs^3
    EFwp = EF/wp                #Def.

    q = k/kF                #Def.

    _gamma0 = gamma0(rs)
    _g0 = g0(rs)

    A = 0.029                                           # (11)
    B =  9./16.*_gamma0 -  3./64.*(1.-_g0) - 16./15.*A    # (12)
    C = -3./ 4.*_gamma0 +  9./16.*(1.-_g0) - 16./ 5.*A    # (13)

    D = 9./16.*_gamma0-9./16.*bm2-3./64.*b0+8./5.*A        #(31e)

    pathologic = numpy.empty_like(q)
    pathologic[q==0.0] = 1.0
    pathologic[q==2.0] = 0.0

    ret = numpy.empty_like(q)

    ret = A*pow(q,4)+B*pow(q,2)+C+(A*pow(q,4)+D*pow(q,2)-C)*pathologic

    return ret

    return a**b

