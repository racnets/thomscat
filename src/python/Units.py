from pint import UnitRegistry
ureg = UnitRegistry()

PhysicalQuantity = ureg.Quantity

import scipy.constants
from scipy.constants import e,c,hbar, m_e, epsilon_0
from scipy.constants import k as kB

ureg.define('angstrom = 1.0e-10 * meter = AA')
ureg.define('rydberg = {0:e} * joule = Ry'.format(scipy.constants.value('Rydberg constant times hc in J')))
ureg.define('electronvolt = {0:e} * joule = eV'.format(scipy.constants.value('electron volt')))
ureg.define('bohr = {0:e} * meter = aB'.format(scipy.constants.value('Bohr radius')))


# Base units.
meter           = PhysicalQuantity(1.0, 'meter')
second          = PhysicalQuantity(1.0, 'second')
kilogram        = PhysicalQuantity(1.0, 'kilogram')
ampere          = PhysicalQuantity(1.0, 'ampere')
volt            = PhysicalQuantity(1.0, 'volt')
joule           = PhysicalQuantity(1.0, 'joule')
newton          = PhysicalQuantity(1.0, 'newton')
kelvin          = PhysicalQuantity(1.0, 'kelvin')
radian          = PhysicalQuantity(1.0, 'radian')
electronvolt    = PhysicalQuantity(1.0, 'electronvolt')
coulomb         = PhysicalQuantity(1.0, 'C')
farad           = PhysicalQuantity(1.0, 'farad')
rydberg         = PhysicalQuantity(1.0, 'rydberg')
bohr            = PhysicalQuantity(1.0, 'bohr')
angstromstar    = PhysicalQuantity(scipy.constants.value('Angstrom star'), 'meter')
angstrom        = PhysicalQuantity(1.0, 'angstrom')
one             = PhysicalQuantity(1.0, 'dimensionless')

e = e*coulomb
c = c*meter/second
hbar = hbar * joule * second
m_e = m_e * kilogram
epsilon0 = epsilon_0*farad/meter
kB = kB*joule/kelvin


def compatible(quant1, quant2):
    return quant1.dimensionality == quant2.dimensionality

