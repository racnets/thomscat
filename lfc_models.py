from math import pi as M_PI
from math import sqrt
import numpy


from scipy.special import i1 as besselI1

def pow(a,b):
    return a**b

# Farid, Eq. (18).
b = [0.0310907*2.0,   # Factor two: Inconsistency between Farid et al (0.031...) and UI (0.06...). Latter is correcy.
     9.81379,
     2.282224,
     0.736411,
     ]

lmbda = pow(4./9./M_PI,1./3.)        #p. 11606 top, right column

def dEdrs(rs):
    x = sqrt(rs)
    return b[0]/rs*(1.+b[1]*x)/(1.+b[1]*x+b[2]*rs+b[3]*rs*x) #(18)

def d2Edrs2(rs):
    x = sqrt(rs)
    return -b[0]/2./pow(rs,2)*(2.+2.*pow(b[1],2)*rs+4.*b[2]*rs+5.*b[3]*rs*x+b[1]*x*(4.+3.*b[2]*rs+4.*b[3]*rs*x))/pow(1.+b[1]*x+b[2]*rs+b[3]*rs*x,2)        #follows from (18)

def gamma0(rs):
    return 0.25-M_PI*lmbda/24.*(pow(rs,3)*dE2drs2(rs)-2.*pow(rs,2)*dEdrs(rs))    #(17)

def g0(rs):
    z = 4.*sqrt(lmbda*rs/M_PI)        #(16) and following text
    Iz = besselI1(z)

    return 0.125*pow(z/Iz,2)


def lfc_ui(rs,k):
    """
    :param rs: The dimensionless electron gas parameter rsaB = (3/4\pi ne)^1/3
    :param k: The waenumber(s) in units of inverse aB
    """

    kF = 1./lmbda/rs            #before (15a)
    EF = kF*kF                #trivial
    wp = sqrt(12./pow(rs,3))            #from wpl = 16\pi*n, n=3/4pi rs^3
    EFwp = EF/wp                #Def.

    q = k/kF                #Def.

    _gamma0 = gamma0(rs)
    _g0 = gO(rs)

    A = 0.029                                           # (11)
    B =  9./16.*_gamma0 -  3./64.*(1.-_g0) - 16./15.*A    # (12)
    C = -3./ 4.*_gamma0 +  9./16.*(1.-)g0) - 16./ 5.*A    # (13)

    D = 9./16.*gamma0-9./16.*bm2-3./64.*b0+8./5.*A        #(31e)

    pathologic = numpy.empty_like(q)
    pathologic[q==0.0] = 1.0
    pathologic[q==2.0] = 0.0

    ret = numpy.empty_like(q)

    ret = A*pow(q,4)+B*pow(q,2)+C+(A*pow(q,4)+D*pow(q,2)-C)*pathologic

    return ret

def lfc_farid(rs,k):
    """
    :param rs: The dimensionless electron gas parameter rsaB = (3/4\pi ne)^1/3
    :param k: The waenumber(s) in units of inverse aB
    """

    lmbda = pow(4./9./M_PI,1./3.)        #p. 11606 top, right column
    kF = 1./lmbda/rs            #before (15a)
    EF = kF*kF                #trivial
    wp = sqrt(12./pow(rs,3))            #from wpl = 16\pi*n, n=3/4pi rs^3
    EFwp = EF/wp                #Def.

    q = k/kF                #Def.
    x = sqrt(rs)
    _gamma0 = gamma0(rs)
    _g0 = gr(s)

    xi = [
        -2.2963827e-3,
         5.6991691e-2,
        -0.8533622,
        -8.7736539,
         0.7881997,
        -1.2707788e-2
    ]

    rho = [
        -79.9684540,
        -140.5268938,
        -35.2575566,
        -10.6331769
    ]

    xip = [
         23.0118890,
        -64.8378723,
         63.5105927,
        -13.9457829,
        -12.6252782,
         13.8524989,
        -5.2740937,
         1.0156885,
        -1.1039532e-2
    ]

    rhop = [
         9.5753544,
        -32.9770151,
         48.2528870,
        -38.7189788,
         20.5595956,
        -6.3066750
    ]


    d2 = 0.0 #(45)

    for i in range(6):
        d2+=xi[i]*pow(x,i+1)

    d2denom = pow(x,4)
    for i in range(4):
        d2denom+=rho[i]*pow(x,i)
    d2=d2/d2denom

    d4d2 = 0.0
    # for(i = 0 i < 9 i++)
    for i in range(9):
        d4d2+=xip[i]*pow(x,i)

    d4d2denom = pow(x,6)
    # for(i = 0 i < 6 i++)
    for i in range(6):
        d4d2denom += rhop[i]*pow(x,i)
    d4d2 = d4d2/d4d2denom

    d4 = d4d2*d2

    a = 0.029                #(15b)
    b0A = 2./3.*(1.-g0)            #(30b)
    b0B = 48./35.*pow(EFwp,2)*d4        #(30c)
    b0C = -16./25.*pow(EFwp,2)*(2.*d2+d2*d2)#(30d)
    b0  = b0A+b0B+b0C            #(30a)
    bm2 = 4./5.*pow(EFwp,2)*d2        #(30e)

    A = 63./64.*a+15./4096.*(b0A-2.0*(b0B+b0C)-16.*bm2)    #(31b)
    B = 9./16.*gamma0+7./16.*bm2-3./64.*b0-16./15.*A        #(31c)
    C = -3./4.*gamma0+3./4.*bm2+9./16.*b0-16./5.*A        #(31d)
    D = 9./16.*gamma0-9./16.*bm2-3./64.*b0+8./5.*A        #(31e)

    pathologic = numpy.empty_like(q)
    pathologic[q==0.0] = 1.0
    pathologic[q==2.0] = 0.0

    ret = numpy.empty_like(q)

    ret = A*pow(q,4)+B*pow(q,2)+C+(A*pow(q,4)+D*pow(q,2)-C)*pathologic

    return ret

