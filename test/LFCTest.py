
""" Test module for the SingFELPhotonDiffractor."""
import os
import h5py
import numpy
import pylab
import shutil
import periodictable as PTE

# Include needed directories in sys.path.
import unittest
from Units import *
from LFC import linspace , Plasma, LFC, Hubbard, Farid, Utsumi1982

class PlasmaTest(unittest.TestCase):
    """
    Test class for the Plasma class.
    """

    @classmethod
    def setUpClass(cls):
        """ Setting up the test class. """
        pass

    @classmethod
    def tearDownClass(cls):
        pass

    def setUp(self):
        """ Setting up a test. """
        self.__files_to_remove = []
        self.__dirs_to_remove = []

    def tearDown(self):
        """ Tearing down a test. """
        for f in self.__files_to_remove:
            if os.path.isfile(f):
                os.remove(f)
        for d in self.__dirs_to_remove:
            if os.path.isdir(d):
                shutil.rmtree(d)

    def testConstruction(self):
        """ Test the construction. """

        plasma = Plasma(elements=[PTE.hydrogen],
                electron_density=1.0e29*meter**-3,
                electron_temperature=10.0*electronvolt/kB,
                average_ionization=1,
                )

        self.assertAlmostEqual(plasma.plasma_frequency.m_as('PHz'), 17.839863646512892 )
        self.assertAlmostEqual(plasma.rs, 2.5256276926207892)
        self.assertAlmostEqual(plasma.Gamma_ee, 1.0774108804936324)

class LinspaceTest(unittest.TestCase):
    """
    Test class for the linspace class.
    """

    @classmethod
    def setUpClass(cls):
        """ Setting up the test class. """
        pass

    @classmethod
    def tearDownClass(cls):
        pass

    def setUp(self):
        """ Setting up a test. """
        self.__files_to_remove = []
        self.__dirs_to_remove = []

    def tearDown(self):
        """ Tearing down a test. """
        for f in self.__files_to_remove:
            if os.path.isfile(f):
                os.remove(f)
        for d in self.__dirs_to_remove:
            if os.path.isdir(d):
                shutil.rmtree(d)

    def testConstruction(self):
        """ Test the various ways of constructing a linspace """

        ls = linspace(0.,100., num=1024)

        self.assertEqual(ls[0], 0.)
        self.assertEqual(ls[-1], 100.)

        self.assertEqual(ls.start, 0.)
        self.assertEqual(ls.stop, 100.)

        self.assertEqual(ls.num, 1024)
        self.assertEqual(len(ls), 1024)

        self.assertAlmostEqual(ls.step, 100./1023)

    def testConstructionPhysicalQuantity(self):
        """ Test constructing a linspace with physical quantities"""

        ls = linspace(0.*meter,100.*meter, num=1024)

        self.assertEqual(ls[0], 0.*meter)
        self.assertEqual(ls[-1], 100.*meter)

        self.assertEqual(ls.start, 0.*meter)
        self.assertEqual(ls.stop, 100.*meter)

        self.assertEqual(ls.num, 1024)
        self.assertEqual(len(ls), 1024)

        self.assertAlmostEqual(ls.step, 100.*meter/1023)

    def testConstructionLengthOne(self):
        """ Test constructing a linspace of size 1 """

        ls = linspace(1.*meter,1.*meter, num=1)

        self.assertEqual(ls[0], ls[-1])
        self.assertEqual(ls.step, 0.0*meter)

class LFCTest(unittest.TestCase):
    """
    Test class for the LFC class.
    """

    @classmethod
    def setUpClass(cls):
        """ Setting up the test class. """
        cls.__plasma = Plasma(elements=[PTE.hydrogen],
                electron_density=1.0e29*meter**-3,
                electron_temperature=10.0*electronvolt/kB,
                average_ionization=1,
                )

    @classmethod
    def tearDownClass(cls):
        del cls.__plasma

    def setUp(self):
        """ Setting up a test. """
        self.__files_to_remove = []
        self.__dirs_to_remove = []

    def tearDown(self):
        """ Tearing down a test. """
        for f in self.__files_to_remove:
            if os.path.isfile(f):
                os.remove(f)
        for d in self.__dirs_to_remove:
            if os.path.isdir(d):
                shutil.rmtree(d)

    def testHubbard(self):
        """ Test constructing a LFC"""

        lfc = LFC(plasma=self.__plasma,
                model=Hubbard,
                wavenumbers=numpy.linspace(0.0, 10.0, 101)/angstrom,
                energies=0.0*electronvolt,
                )

        self.assertTrue(all(lfc._LFC__values < 0.5))

    def testFaridOldSchool(self):
        """ Test the Farid model. """

        from lfc_farid import lfc_farid
        ks = numpy.linspace(0.0001, 10.0, 101)
        lfc = lfc_farid( 2.0, ks)

        pylab.plot(ks, lfc)
        pylab.show()


    def testFarid(self):
        """ Test the Farid model. """

        lfc = LFC(plasma=self.__plasma,
                model=Farid,
                wavenumbers=numpy.linspace(0.0, 10.0, 101)/angstrom,
                energies=0.0*electronvolt,
                )

        self.assertTrue(all(lfc._LFC__values > 0.0))

    def testLFCFaridBenchmark(self):

        lfc = LFC(plasma=self.__plasma,
                model=Farid,
                wavenumbers = numpy.linspace(0.0, 4.*self.__plasma.qF.m_as(1/bohr), 256)/bohr,
                energies=0.0*electronvolt,
                )

        values = lfc.evaluate()

        benchmarks = numpy.load('lfc_farid.npy')

        for i,(v,b) in enumerate(zip(values, benchmarks)):
            print(i,v,b)
            self.assertAlmostEqual(v, b)

    def plotAll(self):
        """ Plot all LFCs. """

        plasma = self.__plasma
        wavenumbers = numpy.linspace(0.0, plasma.qF*bohr*4., 256)/bohr
        energies = 0.0*electronvolt

        models = LFC._static_models()

        from matplotlib import pyplot
        for model in models:
            lfc = LFC(plasma=plasma, wavenumbers=wavenumbers, energies=energies, model=model).evaluate()
            pyplot.plot(wavenumbers/plasma.qF, lfc, label=model.name())

        pyplot.xlabel(r"Wavenumber $q/q_{F}$")
        pyplot.ylabel(r"LFC")
        pyplot.legend(loc=2)

        pyplot.show()

class UnitsTest(unittest.TestCase):
    """
    Test class for the units module.
    """

    @classmethod
    def setUpClass(cls):
        """ Setting up the test class. """
        pass

    @classmethod
    def tearDownClass(cls):
        pass

    def setUp(self):
        """ Setting up a test. """
        self.__files_to_remove = []
        self.__dirs_to_remove = []

    def tearDown(self):
        """ Tearing down a test. """
        for f in self.__files_to_remove:
            if os.path.isfile(f):
                os.remove(f)
        for d in self.__dirs_to_remove:
            if os.path.isdir(d):
                shutil.rmtree(d)

    def testAPI(self):
        """ Test basic operations on units. """

        length = 3.54*meter
        self.assertIsInstance(length, PhysicalQuantity)
        self.assertAlmostEqual(length/angstrom, length/meter*1e10)

        self.assertEqual(length.units, meter)
        self.assertTrue(compatible(length, bohr))

if __name__ == '__main__':
    unittest.main()

