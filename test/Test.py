import unittest
import os, sys

from LFCTest import LinspaceTest
from LFCTest import PlasmaTest


# Define the test suite.
suites = unittest.TestSuite([
            unittest.makeSuite(LinspaceTest, 'test'),
            unittest.makeSuite(PlasmaTest, 'test'),
            unittest.makeSuite(LFCTest, 'test'),
         ])

# Run the top level suite and return a success status code. This enables running an automated git-bisect.
if __name__=="__main__":

    result = unittest.TextTestRunner(verbosity=2).run(suites)

    if result.wasSuccessful():
        print('---> OK <---')
        sys.exit(0)

    sys.exit(1)
